#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <errno.h>
#include <bits/errno.h>
#include <netinet/in.h>
#include <string.h>
#include "ancillary.h"

const int NUM_CHILDS = 5;
const char* COMMAND_READY = "r";
int* childSockets;
int* childPids;
int server_socket;

char *createResponse(char *url);

void logChildMessage(int fd, int num, char* mess){
    char *s = malloc(100);
    sprintf(s,"child num %d: %s\n", num, mess);
    write(fd, s, strlen(s));
}

void handleChildError(int fd, int num, char *mess) {
    logChildMessage(fd, num, "error: ");
    logChildMessage(fd, num, mess);

}

void handleError(char *string) {
    perror(string);
    close(server_socket);
    exit(1);
}

void child_process(int num, int interactSocket)
{
    int outputFd;
    int internetSocket;
    int bufsize = 1024;
    char *buffer = malloc(bufsize);

    if (!ancil_recv_fd(interactSocket, &outputFd)) {
        handleError("ancil_recv_fd");
    } else {
        printf("Received output fd: %d\n", outputFd);
    }
    write(outputFd, "child start work\n", 18);

    while(1) {
        if(write(interactSocket, COMMAND_READY, 1)<0){
            handleChildError(outputFd, num, "write intternet soket:");
        };

        if (!ancil_recv_fd(interactSocket, &internetSocket)) {
            handleChildError(outputFd, num, "ancil_recv_fd");
            continue;
        } else {
            logChildMessage(outputFd, num, "Received internet socket");
        }

        if(recv(internetSocket, buffer, bufsize, 0) == -1){
            handleChildError(outputFd, num, "recv GET query");
            continue;
        }
        logChildMessage(outputFd, num, "recv message:");
        logChildMessage(outputFd, num, buffer);
        char* response = createResponse(buffer);
        if(write(internetSocket, response, strlen(response)) == -1){
            handleChildError(outputFd, num, "write internet socket");
            continue;
        }
        logChildMessage(outputFd, num, "response sent:");
        logChildMessage(outputFd, num, response);
        close(internetSocket);
    }
}

char *createResponse(char *request) {
    char *result = malloc(sizeof(char)*1000);
    strcpy(result, "HTTP/1.1 200 OK\n");
    strcat(result, "Content-length: 46\n");
    strcat(result, "Content-Type: text/html\n\n");
    strcat(result, "<html><body><H1>Hello world</H1></body></html>");
    return result;
}

int findFreeChild();

void runParentProcess() {

    int new_socket;
    int port = 8080;
    socklen_t addrlen;

    struct sockaddr_in address;

    if ((server_socket = socket(AF_INET, SOCK_STREAM, 0)) > 0){
        printf("The socket was created\n");
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    int i = 0;
    for(; i<100; i++) {
        if (bind(server_socket, (struct sockaddr *) &address, sizeof(address)) == 0) {
            printf("Binding Socket, port %d\n", port);
            break;
        } else {
            perror("server: bind failed");
            port++;
            address.sin_port = htons(port);
        }
    }
    if(server_socket == 0){
        handleError("server: bind failed");
    }

    while (1) {
        struct sockaddr_in clientAddress;
        if (listen(server_socket, 10) < 0) {
            perror("server: listen");
            exit(1);
        }

        if ((new_socket = accept(server_socket, (struct sockaddr *) &address, &addrlen)) < 0) {
            perror("server: accept");
            exit(1);
        }

        if (new_socket > 0){
            printf("The Client is connected...\n");
        }

        int childInteractSocket = findFreeChild();

        if(childInteractSocket == 0){
            handleError("all child busy");//todo
        }

        if (ancil_send_fd(childInteractSocket, new_socket)) {
            handleError("ancil_send_fd");
        } else {
            printf("Sent fd.\n");
        }
    }
}

int findFreeChild() {
    char readbuffer[80];
    int i = 0;
    for(; i<NUM_CHILDS; i++){
        int childSock = childSockets[i];
        if (read(childSock, readbuffer, 10) < 0){
            if(errno != EWOULDBLOCK){
                handleError("error unblock read");
            }
        } else {
            printf("child %d free\n", i);
            char subbuff[2];
            memcpy( subbuff, readbuffer, 1 );
            subbuff[1] = '\0';
            if(strcmp(subbuff, COMMAND_READY)==0) {
                return childSock;
            }
        }
    }
    return 0;
}



void setSocketToNonBlocking(int sock);

void initProcessSet(){
    int i = 0;
    for(; i<NUM_CHILDS; i++){
        int sock[2];
        if(socketpair(PF_UNIX, SOCK_STREAM, 0, sock)) {
            handleError("socketpair");
        } else {
            printf("Established socket pair: (%d, %d)\n", sock[0], sock[1]);
        }
        int pid;
        switch(pid = fork()) {
            case 0:
                close(sock[0]);
                child_process(i, sock[1]);
                break;
            case -1:
                handleError("fork");
            default:
                if (ancil_send_fd(sock[0], 1)) {
                    handleError("ancil_send_fd");
                }
                childSockets[i] = sock[0];
                setSocketToNonBlocking(sock[0]);
                close(sock[1]);
                childPids[i] = pid;
                break;
        }
    }
}

int main(void)
{
    childSockets = malloc(sizeof(int)*NUM_CHILDS);
    childPids = malloc(sizeof(int)*NUM_CHILDS);


    initProcessSet();
    runParentProcess();

}


void setSocketToNonBlocking(int sock) {
    int flags;
    if ((flags = fcntl(sock, F_GETFL, 0)) < 0) {
        handleError("socket ctrl");
    }
    if (fcntl(sock, F_SETFL, flags | O_NONBLOCK) < 0) {
        handleError("socket ctrl");
    }
}

char* itoa(int i, char b[]){
    char const digit[] = "0123456789";
    char* p = b;
    if(i<0){
        *p++ = '-';
        i *= -1;
    }
    int shifter = i;
    do{ //Move to where representation ends
        ++p;
        shifter = shifter/10;
    }while(shifter);
    *p = '\0';
    do{ //Move back, inserting digits as u go
        *--p = digit[i%10];
        i = i/10;
    }while(i);
    return b;
}
